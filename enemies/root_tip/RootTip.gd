extends Polygon2D


export var wave_strength = 0.6
export var wave_delay = 1.0
export var wave_speed = 1.0

export var damage = 100.0
export var health = 10.0
func get_damage():
	return damage

var Player
var col_poly
var Skel
var Root
var Bottom
var Lower
var Middle
var Upper
var Tip
var Point
var Hitbox

func _ready():
	Player = get_tree().get_root().get_node("World").get_node("Player")
	Skel = $Skeleton2D
	Root = $Skeleton2D/Root
	Bottom = $Skeleton2D/Root/Bottom
	Lower = $Skeleton2D/Root/Bottom/Lower
	Middle = $Skeleton2D/Root/Bottom/Lower/Middle
	Upper = $Skeleton2D/Root/Bottom/Lower/Middle/Upper
	Tip = $Skeleton2D/Root/Bottom/Lower/Middle/Upper/Tip
	Point = $Skeleton2D/Root/Bottom/Lower/Middle/Upper/Tip/Point
	Hitbox = $Skeleton2D/Root/Bottom/Lower/Middle/Upper/Tip/Point/Hitbox
	
	Hitbox.connect("body_entered", self, "on_body_entered")
	Hitbox.connect("area_entered", self, "on_area_entered")
	
func _physics_process(delta):
	wave(delta)
	pass

var time = 0.0
func wave(delta):
	Lower.rotation = sin(time) * wave_strength
	Middle.rotation = sin(time - (1 * wave_delay)) * wave_strength
	Upper.rotation = sin(time - (2 * wave_delay)) * wave_strength
	Tip.rotation = sin(time - (3 * wave_delay)) * wave_strength
	time += delta * wave_speed


var timer = 0
func aim_at(node, vec, determination, delta, offset_angle):
	timer = (timer + 1) % 20
	var theta = node.get_global_transform().get_rotation()
	var pos = node.get_global_transform().get_origin()
	var target = (pos.angle_to_point(vec) + offset_angle) * determination
	#if(timer == 0): print(theta, " | ", Player.get_global_transform().get_origin(), " | ", vec, " | ", target)
	node.rotate((target - theta) * delta)	
	
	
func aim_at_player(delta):
	var target = Player.get_global_transform().get_origin()
	
	aim_at(Bottom, target, 0.2, delta, -PI / 2)
	aim_at(Lower, target, 0.4, delta * 8, -PI / 2)
	aim_at(Middle, target, 0.6, delta * 4, -PI / 2)
	aim_at(Upper, target, 0.8, delta * 4, -PI / 2)
	aim_at(Tip, target, 1, delta, -PI / 2)
	
	
	
func on_body_entered(body):
	if(body.has_method("damage")):
		#print("Damaging: ", body)
		body.damage(Hitbox, get_damage(), -wave_speed * Upper.rotation)


func on_area_entered(area):
	if(area.has_method("damage_enemy")):
		health -= area.damage_enemy(self)
		if(health <= 0.0): die()

export var enemy_name = "root_tip"

signal root_tip_died

func die():
	hide()
	Hitbox.disconnect("body_entered", self, "on_body_entered")
	Hitbox.disconnect("area_entered", self, "on_area_entered")
	emit_signal("root_tip_died")
