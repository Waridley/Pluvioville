extends KinematicBody2D

class_name Player


const GRAVITY_VEC = Vector2(0, 3000.0)
const TERMINAL_VEL = Vector2(4000.0, 4000.0)
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
export var walk_speed = 400 # pixels/sec
export var jump_speed = 1000
const SIDING_CHANGE_SPEED = 10
const BULLET_VELOCITY = 1000
const SHOOT_TIME_SHOW_WEAPON = 0.2

export var knockback_scale = 10
export var max_health = 300
export var flappy_mode = false
var health
export var can_attack = false
var atk_time_remaining = 0.0
export var can_sec_attack = false

var linear_vel = Vector2()
export var max_jumps = 1
var remaining_jumps = max_jumps

export var invulnerability_time = 1.5
var remaining_inv_seconds = invulnerability_time

var sat
var one_more_chance = true
onready var midsection = $Skeleton2D/LowerBody/Midsection
onready var upper_body = $Skeleton2D/LowerBody/Midsection/UpperBody
onready var neck = $Skeleton2D/LowerBody/Midsection/UpperBody/Neck
onready var initial_max_walk_speed = walk_speed
onready var initial_max_jump_speed = jump_speed
onready var camera = $Camera2D


var optimistic_texture = load("res://player/Player.svg")
var meh_texture = load("res://player/Player_meh.svg")
var sad_texture = load("res://player/Player_sad.svg")
var shock_texture = load("res://player/Player_shock.svg")

func _ready():
	reset()

# cache the sprite here for fast access (we will set scale to flip it often)
# onready var sprite = $Shape/Sprite
# cache bullet for fast access
# var Bullet = preload("res://player/Bullet.tscn")
func reset():
	position = Vector2()
	linear_vel = Vector2()
	one_more_chance = true
	health = max_health
	sat = health / 1000.0
	set_texture()
	$BodyPolygon.scale.x = 1
	camera.smoothing_speed = 100.0
	camera.offset_h = 1.0
	(material as ShaderMaterial).set_shader_param("brightness", 1.0)
	(material as ShaderMaterial).set_shader_param("saturation", sat)

func die():
	reset()

func set_texture():
	if(remaining_inv_seconds > 0):
		(material as ShaderMaterial).set_shader_param("texture", shock_texture)
	elif(health > 600):
		(material as ShaderMaterial).set_shader_param("texture", optimistic_texture)
	elif(health > 100):
		(material as ShaderMaterial).set_shader_param("texture", meh_texture)
	else:
		(material as ShaderMaterial).set_shader_param("texture", sad_texture)

func _physics_process(delta):
	### RESET ###
	if(position.y > 8000): reset()
	
	### MOVEMENT ###
	
	# Apply gravity
	linear_vel.y = min(linear_vel.y + (delta * GRAVITY_VEC.y), TERMINAL_VEL.y)
	linear_vel.x = min(linear_vel.x + (delta * GRAVITY_VEC.x), TERMINAL_VEL.x)
	# Move and slide
	linear_vel = move_and_slide(linear_vel, FLOOR_NORMAL, SLOPE_SLIDE_STOP)
	# Detect if we are on floor - only works if called *after* move_and_slide
	
	var on_floor = is_on_floor()
	
	### CONTROL ###
	# Horizontal movement
	var target_speed = 0
	if Input.is_action_pressed("move_right"):
		target_speed += 1
		#$BodyPolygon.scale.x = 1
		$Skeleton2D/LowerBody.scale.x = 1
	elif Input.is_action_pressed("move_left"):
		target_speed -= 1
		#$BodyPolygon.scale.x = -1
		$Skeleton2D/LowerBody.scale.x = -1
	camera.offset_h = $Skeleton2D/LowerBody.scale.x
	if(Input.is_action_pressed("look_up")):
		neck.rotate((($BodyPolygon.scale.x * -TAU/12.0) - neck.rotation) * 0.05)
	if(Input.is_action_pressed("look_down")):
		neck.rotate((($BodyPolygon.scale.x * TAU/12.0) - neck.rotation) * 0.05)
	else:
		neck.rotate(-neck.rotation * 0.05)
	if(Input.is_action_pressed("crouch")):
		midsection.translate(Vector2(0, -midsection.transform.origin.y * 0.05))
		walk_speed = initial_max_walk_speed * 0.5
		jump_speed = initial_max_jump_speed * 0.5
		$Shape.scale.y = $Shape.scale.y - (($Shape.scale.y - 0.6) * 0.05)
		$Shape.transform.origin = upper_body.get_relative_transform_to_parent(self).origin + Vector2(0, -80.0)
	else:
		midsection.translate(Vector2(0, (-256.0 - midsection.transform.origin.y) * 0.1))
		walk_speed = initial_max_walk_speed
		jump_speed = initial_max_jump_speed
		$Shape.transform.origin = upper_body.get_relative_transform_to_parent(self).origin
		$Shape.scale.y += ((1 - $Shape.scale.y) * 0.1)
	if(can_attack and Input.is_action_just_pressed("primary_attack")):
		primary_attack()
	if(can_sec_attack and Input.is_action_just_pressed("secondary_attack")):
		secondary_attack()
		
	
	target_speed *= walk_speed
	linear_vel.x = lerp(linear_vel.x, target_speed, 0.05)
	
	
	if(on_floor): 
		remaining_jumps = max_jumps
		var target_rotation = lerp(-TAU/64.0, TAU/64.0, inverse_lerp(-walk_speed, walk_speed, linear_vel.x))
		rotate((target_rotation - rotation) * 0.1)
		camera.offset_v = -8 # For some reason this works... like it's being divided by 10
		if(linear_vel.x > 50 or linear_vel.x < -50): $Skeleton2D/LowerBody/DirtParticles.emitting = true
		else: $Skeleton2D/LowerBody/DirtParticles.emitting = false
	else:
		$Skeleton2D/LowerBody/DirtParticles.emitting = false
		rotate(-rotation * 0.01)
	
	camera.drag_margin_left = 0.8 - ease(inverse_lerp(0.0, TERMINAL_VEL.y, linear_vel.y) * 0.8, 1)
	camera.drag_margin_right = 0.8 - ease(inverse_lerp(0.0, TERMINAL_VEL.y, linear_vel.y) * 0.8, 1)
	camera.drag_margin_bottom = 0.8 - inverse_lerp(0.0, TERMINAL_VEL.y, linear_vel.y)
	camera.smoothing_speed = max(abs(linear_vel.y) / 400.0, 1.1)
	
	
	# Jumping
	if (remaining_jumps > 0 or flappy_mode) and Input.is_action_just_pressed("jump"):
		linear_vel.y = -jump_speed
		remaining_jumps -= 1
		#($SoundJump as AudioStreamPlayer2D).play()
	
	var prev_inv = remaining_inv_seconds
	remaining_inv_seconds -= delta
	if(prev_inv > 0.0 and remaining_inv_seconds <= 0.0): set_texture()
	
	### ANIMATION ###
	var MAX = TERMINAL_VEL.y
	var x = linear_vel.y
	if(atk_time_remaining < 0.0):
		$Skeleton2D/LowerBody/Midsection/UpperBody/RightArm.rotation = lerp_angle(TAU/16.0, TAU/2.2 , ease(smoothstep(-MAX, MAX, -x), 0.5))
	$Skeleton2D/LowerBody/Midsection/UpperBody/LeftArm.rotation = TAU - $Skeleton2D/LowerBody/Midsection/UpperBody/RightArm.rotation
	
	atk_time_remaining -= delta

func damage(source, amount, velocity):
	if(remaining_inv_seconds <= 0.0):
		remaining_inv_seconds = invulnerability_time
		var direction = global_position - source.global_position
		linear_vel.x = sign(direction.x) * (knockback_scale * amount + velocity * knockback_scale)
		linear_vel.y = min(linear_vel.y, -knockback_scale * 0.02 * amount - knockback_scale)
		health -= amount
		sat = clamp(health, 0.0, 1000.0) / 1000.0
		(material as ShaderMaterial).set_shader_param("saturation", sat)
		set_texture()
		if(health <= 0.0):
			if(one_more_chance):
				(material as ShaderMaterial).set_shader_param("brightness", 0.6)
				one_more_chance = false
			else:
				die()

func on_pickup_prim_weap():
	$RightLeafPolygon.show()
	can_attack = true

func primary_attack():
	atk_time_remaining = 0.5
	var speed = 1.0
	if(health <= 0.0):
		speed = 0.6
	$AnimationPlayer.play("Strike", -1, speed)

func secondary_attack():
	print("Attacking secondarily!")
