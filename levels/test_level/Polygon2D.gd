extends Polygon2D

var col_poly = CollisionPolygon2D.new()

func _ready():
	update_collision()
	pass

func _set(property: String, value):
	.set(property, value)
	if property == "polygon" :
		print("Updating collision")
		update_collision()
	
func update_collision():
	col_poly.polygon = polygon
