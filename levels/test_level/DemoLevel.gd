extends Node2D

func _ready():
	$Player/RightLeafPolygon.hide()
	$"Terrain/Small Floating/HealthPack".connect("body_entered", self, "on_player_entered")
	$Terrain/BossPlatform/RootTip8.connect("root_tip_died", self, "on_boss_died")
	
func on_player_entered(body):
	if("max_health" in body):
		body.max_health = 1000
		body.health = 1000
		body.set_texture()
		body.material.set_shader_param("saturation", 1.0)

func on_boss_died():
	$Player.flappy_mode = true
	(material as ShaderMaterial).set_shader_param("saturation", 1.0)
	$Rain.material.set_shader_param("minimum", 0.1)
	$Rain.material.set_shader_param("variance", 0.4)
