extends Area2D

func _ready():
	connect("body_entered", self, "on_hitbox_entered")

func on_hitbox_entered(body):
	if($Sprite.is_visible() and body.has_method("on_pickup_prim_weap")):
		body.on_pickup_prim_weap()
		$Sprite.hide()
