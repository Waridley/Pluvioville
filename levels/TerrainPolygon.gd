extends Polygon2D

onready var col_poly = $".." as CollisionPolygon2D
export var always_update_collision = false

func _ready():
	update_collision()
	pass

func _physics_process(delta):
	if(always_update_collision):
		update_collision()

func update_collision():
	col_poly.polygon = polygon
