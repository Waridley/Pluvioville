extends StaticBody2D

export var reload_shape = true

func _physics_process(_delta):
	if(reload_shape):
		$CollisionPolygon2D.polygon = $Polygon2D.polygon
		reload_shape = false
